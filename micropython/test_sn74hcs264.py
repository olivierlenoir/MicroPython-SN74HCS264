# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2021-03-20 10:19:03
# License: MIT, Copyright (c) 2021 Olivier Lenoir
# Language: MicroPython v1.14
# Project: Test SN74HCS264
# Description: MicroPython Driver for SN74HCS264


from sn74hcs264 import SN74HCS264
from machine import Pin


a = Pin(2, Pin.OUT)
b = Pin(4, Pin.OUT)
clk = Pin(16, Pin.OUT)
clr = Pin(17, Pin.OUT)

# PulseView trigger
trigger = Pin(23, Pin.OUT)

register = SN74HCS264(a, b, clk, clr)

trigger(1)
register.clear()
register.write(37)
register.clear()
trigger(0)

