# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2021-03-20 10:00:08
# License: MIT, Copyright (c) 2021 Olivier Lenoir
# Language: MicroPython v1.14
# Project: SN74HCS264
# Description: MicroPython Driver for SN74HCS264


from machine import Pin


class SN74HCS264(object):
    """Driver for SN74HCS264 8-Bit Parallel-Out Serial Shift Registers
    With Schmitt-Trigger Inputs and Inverted Outputs"""

    def __init__(self, a_pin, b_pin, clk_pin, clr_pin):
        self.a = a_pin
        self.b = b_pin
        self.clk = clk_pin
        self.clr = clr_pin
        self.reset()

    def write(self, data):
        self.b(1)
        for offset in range(8):
            stat = 0 if data & (1 << offset) else 1
            self.a(stat)
            self._clk()
        self.b(0)

    def clear(self):
        self.clr(0)
        self.clr(1)

    def _clk(self):
        self.clk(0)
        self.clk(1)

    def reset(self):
        self.a(1)
        self.b(0)
        self.clk(1)
        self.clear()
